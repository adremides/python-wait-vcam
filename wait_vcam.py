# !/usr/bin/env python3

__author__ = ['Yamil Jaskolowski', '[Adremides](https://adremides.com.ar)']
__date__ = '2020.12.21'

import threading
import time
from urllib import error, request

import ffmpeg
from inotify import adapters


class VirtualCamStandBy:
    ipwebcam_url = 'http://192.168.20.62:8080/video'
    is_ipwebcam_online = False
    id_device = '2'
    video_device = f'video{id_device}'
    virtual_device = f'/dev/{video_device}'
    virtual_device_sys_dir = f'/sys/devices/virtual/video4linux/{video_device}'
    watcher = adapters.Inotify()
    stream_image_process = None
    stream_ipwebcam_process = None
    checking_ipwebcam = None
    watching_inodes = None
    streaming_image = None
    streaming_ipwebcam = None

    def __init__(self):
        self.start_check_ipwebcam()
        self.start_watch()

    def start_watch(self):
        self.watching_inodes = threading.Thread(target=self.watch_dev)
        self.watching_inodes.start()

    def watch_dev(self):
        self.watcher.add_watch(self.virtual_device)

        for event in self.watcher.event_gen(yield_nones=False):
            (_, type_names, path, filename) = event

            if type_names:
                if type_names[0] == 'IN_OPEN':
                    if not self.is_streaming() and not self.stream_image_process:
                        self.start_stream_image()

    def start_check_ipwebcam(self):
        self.checking_ipwebcam = threading.Thread(target=self.check_ipwebcam_online)
        self.checking_ipwebcam.start()

    def check_ipwebcam_online(self):
        while True:
            try:
                time.sleep(2)
                response = request.urlopen(self.ipwebcam_url, timeout=1)
                if response.getcode() == 200:
                    self.stop_stream_image()
                    self.streaming_image = None
                    self.is_ipwebcam_online = True
                    time.sleep(2)
                    if not self.streaming_ipwebcam:
                        self.start_stream_ipwebcam()
                else:
                    self.is_ipwebcam_online = False
                    self.streaming_ipwebcam = None

            except error.URLError:
                self.is_ipwebcam_online = False
                self.streaming_ipwebcam = None

    def is_streaming(self):
        try:
            device_format = open(f'{self.virtual_device_sys_dir}/format', 'r')
            # Si tiene guardado un formato es porque algo está transmitiendo
            video_format = device_format.read()
            return bool(video_format)
        except Exception as e:
            print(e)

    def start_stream_image(self):
        self.streaming_image = threading.Thread(target=self.stream_image)
        self.streaming_image.start()

    def stop_stream_image(self):
        if self.stream_image_process:
            # self.stream_image_process.communicate(str.encode("q"))  # Equivalent to send a Q
            self.stream_image_process.terminate()
            self.stream_image_process = None
            print('stream image stopped')

    def stream_image(self, pix_format='yuv420p', img_uri='video-no-disponible.png'):
        self.stream_image_process = (
            ffmpeg
            .input(img_uri, loop='1')
            .output(
                self.virtual_device,
                format="v4l2",
                vcodec="rawvideo",
                pix_fmt=pix_format
            )
            .global_args("-re")  # argument to act as a live stream
        )
        self.stream_image_process = self.stream_image_process.run_async(overwrite_output=True)

    def start_stream_ipwebcam(self):
        self.streaming_ipwebcam = threading.Thread(target=self.stream_ipwebcam)
        self.streaming_ipwebcam.start()

    def stream_ipwebcam(self, pix_format='yuv420p'):
        self.stream_ipwebcam_process = (
            ffmpeg
            .input(
                self.ipwebcam_url,
            )
            .output(
                self.virtual_device,
                format='v4l2',
                map='0',
                vcodec="rawvideo",
                pix_fmt=pix_format
            )
            .global_args("-re")  # argument to act as a live stream
        )
        self.stream_ipwebcam_process = self.stream_ipwebcam_process.run_async(overwrite_output=True)


if __name__ == '__main__':
    main = VirtualCamStandBy()
