# wait-vcam

This script is used with IPWebCam android app and a v4l2loopback device.

It stream a image to a v4l2loopback device while device is requested and nothing is beign streamed at it. When the app video is detected it stops the stream of the image and start streaming the actual video from the app.

For this to work edit the `ipwebcam_url` variable with the address of the video (something like http://IP_ANDROID:8080/video), this other variable `id_device` with the number of the v4l2loopback device (if the device is in /dev/video1) then `id_device = 1`.

# Requirements
Python 3.8  
ffmpeg-python `pip3 install ffmpeg-python`  
inotify `pip3 install inotify`  
Android device with IP WebCam app installed

# Usage
``` bash
$ python3 wait_vcam.py
```

# Notes
This was tested and configured to work with a cam in 1280x720 (image resolution).
Tested in Kubuntu 20.04.

